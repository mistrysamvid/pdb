package main

import (
	"bufio"
	"container/list"
	"encoding/json"
	"fmt"
	tuiGo "github.com/marcusolsson/tui-go"
	"log"
	"net"
	"os"
	"pd-server/tui"
	"pd-utils"
	"strconv"
	"strings"
	"sync"
)

var connections = make(map[int]*net.TCPConn)

// This variable is just used to stick the process in an infinite loop
// so that the program will wait until we attach a debugger to it
var a bool = true
var logger *log.Logger
var loopMode = false
var shouldContinue = true

type CollectiveCall struct {
	funcName string
	callers  map[int]*utils.CollectiveInfo
}

var collectiveCallList struct {
	calls *list.List
	mux   sync.Mutex
}

func main() {

	/*for a {
		continue
	}*/
	a = false
	numPorts := 1

	if len(os.Args) > 1 {
		for _, arg := range os.Args {
			if arg == "-l" {
				// Loop mode is this really cool functionality where the debugger will keep listening
				// for new connections after the previously connected processes have exited gracefully.
				// It will automatically stop the execution of a program whenever there is an error
				// because there is a breakpoint on our custom error handler.
				// This feature is very useful when the bug is not always reproducible and it occurs
				// only sometimes. You can run the client process in a script which launches the process
				// repeatedly and the debugger will keep connecting and disconnecting to the processes,
				// stopping only when any error is encountered in the program.
				loopMode = true
			} else if strings.HasPrefix(arg, "-p") {
				ports, err := strconv.Atoi(arg[2:])
				if err != nil {
					panic("Non Integer argument provided for number of ports.")
				} else {
					numPorts = utils.Max(numPorts, ports)
				}
			}
		}
	}

	f, err := os.Create("debug.log")
	if err != nil {
		panic(err)
	}
	defer f.Close()
	logger = log.New(f, "", log.LstdFlags)
	tuiGo.SetLogger(logger)

	// Initialize some structs.
	collectiveCallList.mux.Lock()
	collectiveCallList.calls = list.New()
	collectiveCallList.mux.Unlock()

	ports := make([]int, numPorts)
	host := "0.0.0.0"

	// Listen on specified number of ports
	for i := 0; i < numPorts; i++ {
		ports[i] = 8080 + i
	}

	localAddresses := make([]*net.TCPAddr, numPorts)
	for i, port := range ports {
		localAddresses[i], err = net.ResolveTCPAddr("tcp", fmt.Sprintf("%s:%d", host, port))
		if err != nil {
			panic(err)
		}
	}

	connectionsChannel := make(chan *net.TCPConn)

	// Listen on all ports in separate Go routines, pass the connection in a single
	// channel which handles all connection requests
	for i, address := range localAddresses {
		go func(connChannel chan *net.TCPConn, address *net.TCPAddr, port int) {
			ln, err := net.ListenTCP("tcp", address)
			log.Printf("Server running on port %d\n", port)
			utils.CheckError(err)
			for {
				conn, err := ln.AcceptTCP()
				utils.CheckError(err)
				connChannel <- conn
			}
		}(connectionsChannel, address, ports[i])
	}

	for shouldContinue {
		conn := <-connectionsChannel
		handleConnection(conn)
	}
}

// Parse commands.
// There are two types of commands that can be parsed right now.
// 1. Normal GDB Commands
// 2. PDB-specific commands of the format pdb_<command> [r=g,g,g,g] where g
//    can be a single rank, or something like x..y, where x < y.
func parseInput(input string) (command string, ranks []int) {
	input = strings.TrimSpace(input)
	command = input
	ranks = nil

	// If it's a normal gdb command, return immediately.
	if !strings.HasPrefix(command, "pdb_") {
		return
	}

	// Split on any number of spaces or tabs, not just single " ".
	commandSegments := strings.Fields(input)

	// Get the last bit with r=g,g,g,g
	rankSpecString := strings.TrimSpace(commandSegments[len(commandSegments)-1])
	rankSpecString = strings.Trim(rankSpecString, "[]")

	// Get the <command> by stripping away pdb_ and [r=g,g,g,g]
	commandSegments = commandSegments[:len(commandSegments)-1]
	command = strings.Join(commandSegments, " ")
	command = strings.TrimPrefix(command, "pdb_")

	// Using r=g,g,g,g, come up with a list of ranks that have to be
	// sent the command. Note that existence of these ranks is not
	// guaranteed, we just make up the list of ranks based on input.
	rankListPrefix := strings.Split(rankSpecString, "=")[0]
	if rankListPrefix != "r" {
		logger.Println("Rank list prefix other than r is not supported, so sending to all ranks silently.")
		return
	}

	rankGroups := strings.Split(strings.Split(rankSpecString, "=")[1], ",")
	rankSet := make(map[int]bool) // Go's approximation of a set.
	for _, rg := range rankGroups {
		// First check if g is one single rank.
		rank, err := strconv.Atoi(rg)
		if err != nil {
			rgEndpoints := strings.Split(rg, "..")
			if len(rgEndpoints) != 2 {
				continue
			}

			low, err := strconv.Atoi(rgEndpoints[0])
			if err != nil {
				continue
			}

			high, err := strconv.Atoi(rgEndpoints[1])
			if err != nil {
				continue
			}

			for i := low; i < high; i++ {
				rankSet[i] = true
			}

		} else {
			rankSet[rank] = true
		}
	}

	ranks = make([]int, len(rankSet))
	i := 0
	for k := range rankSet {
		ranks[i] = k
		i++
	}
	return
}

func handleConnection(c *net.TCPConn) {
	status, err := bufio.NewReader(c).ReadString('\n')
	utils.CheckError(err)

	status = strings.TrimSpace(status)
	rank, err := strconv.Atoi(strings.Split(status, ",")[0])
	utils.CheckError(err)
	wSize, err := strconv.Atoi(strings.Split(status, ",")[1])
	utils.CheckError(err)
	log.Printf("Processing client with rank = %d, world size = %d, total connections = %d\n", rank, wSize, len(connections))

	connections[rank] = c

	if wSize == len(connections) {
		fmt.Printf("All the clients are connected\n")
		for _, v := range connections {
			fmt.Fprintf(v, "COMMAND:All clients, including you, are connected\n")
		}

		ranks := make([]int, 0, len(connections))
		for r := range connections {
			ranks = append(ranks, r)
		}

		t := tui.NewTUI(ranks, logger)
		t.DrawUI()
		t.ShowMessagesAll("You are connected")
		t.Input.OnSubmit(func(e *tuiGo.Entry) {
			// t.ShowUserInputAll(e.Text())
			takeUserInput(e.Text(), t)
			t.AddToCmdHistory(e.Text())
			t.Input.SetText("")
		})

		processClient := make(chan bool)
		go processClientMessage(wSize, processClient, t)
		if loopMode {
			// If we are in loop mode then start the execution of process automatically
			takeUserInput("c", t)
		}
		<-processClient
		// Returning from this function means all client processes have ended. Prepare
		// for new incoming conenctions.
		connections = make(map[int]*net.TCPConn)
	}

}

func takeUserInput(input string, t *tui.TUI) {
	if len(connections) == 0 {
		t.Quit()
		log.Println("No processes are alive, so quitting")
		os.Exit(0)
	}

	if input == "pdb_listcoll" {
		// List collective call info
		calls := pendingCollectiveInfo()
		go prettyPrintCollectiveInfo(calls, t)
	} else if strings.HasPrefix(input, "vars") {
		// Show variable info window.
		// It takes one argument, the rank whose variables are to be displayed
		vals := strings.Split(input, " ")
		if len(vals) < 2 {
			return
		}
		rankForWindow, err := strconv.Atoi(vals[1])
		if err != nil {
			return
		}
		if rankForWindow >= 0 {
			t.EnableInternalWindow("vars", rankForWindow)
		} else {
			// If a negative value is provided as argument, close the window
			t.DisableInternalWindow("vars")
		}
	} else if input == "quit" {
		shouldContinue = false
		t.Quit()
		for _,c := range connections {
			c.Close()
		}
	} else if strings.HasPrefix(input, "swap") {
		vals := strings.Split(input, " ")
		newRank, _ := strconv.Atoi(vals[1])
		oldRank, _ := strconv.Atoi(vals[2])
		t.Swap(newRank, oldRank)
	} else if strings.HasPrefix(input, "add") {
		vals := strings.Split(input, " ")
		rank, _ := strconv.Atoi(vals[1])
		t.Add(rank)
	} else if strings.HasPrefix(input, "remove") {
		vals := strings.Split(input, " ")
		rank, _ := strconv.Atoi(vals[1])
		t.Remove(rank)
	} else if strings.HasPrefix(input, "pdb_trackcoll") {
		// Enable/Disable tracking of a particular collective call
		toggleCollective(strings.Split(input, " ")[1])
	} else if strings.HasPrefix(input, "f ") {
		// Focus on a window.
		// Takes one argument, the number of window on which to focus.
		// The window numbers are specified on the top of every window in parantheses.
		vals := strings.Split(input, " ")
		if len(vals) < 2 {
			return
		}
		windowNumber, err := strconv.Atoi(vals[1])
		if err != nil {
			return
		}
		t.ChangeFocusedWindow(windowNumber)
	} else if input == "pdb_" + utils.StopNonblockingModification ||
		input == "pdb_" + utils.StartNonblockingModification {
		// This is to Disable/Enable the tracking of modifications to
		// a region of memory which is currently under nonblocking transfer
		command, ranks := parseInput(input)
		logger.Printf("Sent %s to clients", command)
		sendMsgTo(command, ranks, "CONFIG")
	} else {
		// Else this command is for GDB, pass it as it is to the clients
		command, ranks := parseInput(input)
		sendCommandTo(command, ranks)
		t.ShowUserInputClients(command, ranks)
	}
}

func sendCommandTo(message string, ranks []int) {
	sendMsgTo(message, ranks, "RUN")
}

func toggleCollective(coll string) {
	sendMsgTo(coll, nil, "COLLECTIVE")
}

// Send the `message` to ranks specified inside `ranks`.
// If `ranks` is nil, then send message to all the connected clients.
// In case we are trying to send a message to some non-existent client,
// ignore that silently.
// The sent message is of the form <prefix>:<message>
func sendMsgTo(message string, ranks []int, prefix string) {
	if ranks != nil {
		for _, rank := range ranks {
			c, rankExists := connections[rank]
			if !rankExists {
				continue
			}
			fmt.Fprintf(c, "%s", fmt.Sprintf("%s:%s\n", prefix, message))
		}
		return
	}

	for _, c := range connections {
		fmt.Fprintf(c, "%s", fmt.Sprintf("%s:%s\n", prefix, message))
	}
}

func processClientMessage(wSize int, processClientDone chan bool, t *tui.TUI) {
	if wSize != len(connections) {
		return
	}

	var waitGroup sync.WaitGroup
	waitGroup.Add(wSize)

	for r, c := range connections {

		// handling output of every client in a separate go routine
		go func(r int, c *net.TCPConn) {
			defer waitGroup.Done()
			scanner := bufio.NewScanner(c)
			for scanner.Scan() {
				utils.CheckError(scanner.Err())
				line := scanner.Text()
				lineSplit := strings.SplitN(line, ":", 2)
				logger.Printf("Message is %s", line)
				if len(lineSplit) < 2 {
					continue
				}
				handleClientMessage(lineSplit[0], lineSplit[1], r, t)
			}
		}(r, c)
	}

	waitGroup.Wait()
	processClientDone <- true
}

func handleClientMessage(cat string, msg string, rank int, t *tui.TUI) {
	switch cat {
	case "CONSOLE":
		// fmt.Printf("[rank %d] %s\n", rank, msg)
		t.ShowMessagesClient(msg, rank)
	case "ERROR":
		// fmt.Printf("[rank %d] (!) %s\n", rank, msg)
		t.ShowMessagesClient(msg, rank)
	case "COLLECTIVE":
		var coll utils.CollectiveInfo
		_ = json.Unmarshal([]byte(msg), &coll)
		trackCollective(coll)
	case "FRAMEVARS":
		// Received variable information from a client
		var jsonResponse map[string]interface{}
		err := json.Unmarshal([]byte(msg), &jsonResponse)
		rank, _ := strconv.Atoi(fmt.Sprintf("%v", jsonResponse["RANK"]))
		if err != nil {
			println("Error processing json for FRAMEVARS")
			return
		}
		variables := jsonResponse["VALUE"].([]interface{})
		var variableInfo []tui.VariableInfo
		for _, inter := range variables {
			castedInter := inter.(map[string]interface{})
			var value = ""
			if val, ok := castedInter["value"]; ok {
				value = fmt.Sprintf("%v", val)
			}
			variableInfo = append(variableInfo, tui.VariableInfo{
				Name:          fmt.Sprintf("%v", castedInter["name"]),
				VarType:       fmt.Sprintf("%v", castedInter["type"]),
				ValueOptional: value,
			})
		}

		t.AddVariables(rank, variableInfo)

		info, _ := t.GetVariables(rank)
		logger.Println(info)
		if t.IsInternalWindowEnabled("vars") {
			t.RunOnUiThread(func() {
				t.ReDraw()
			})
		}

		if fName, ok := jsonResponse["FUNC"]; ok {
			// If the function in which the breakpoint was hit is "error_handler", then it
			// means there was an error in one of the ranks, add the window of that rank on
			// the screen.
			if fName == "error_handler" {
				t.RunOnUiThread(func() {
					t.Add(rank)
				})
			}
		}
	case "EXITINFO":
		// info when a client process has exited
		if !loopMode {
			// If we're not in loop mode, then we don't care
			return
		}

		var jsonResponse map[string]interface{}
		err := json.Unmarshal([]byte(msg), &jsonResponse)
		rank, _ := strconv.Atoi(fmt.Sprintf("%v", jsonResponse["RANK"]))
		if err != nil {
			println("Error processing json for EXITINFO")
			return
		}
		connections[rank].Close()
		delete(connections, rank)
		if len(connections) == 0 {
			// if we are in loop mode and if all connections are closed,
			// close the TUI and start waiting for new connections.
			t.Quit()
		}
	}
}

func trackCollective(info utils.CollectiveInfo) {
	collectiveCallList.mux.Lock()
	defer collectiveCallList.mux.Unlock()
	cl := collectiveCallList.calls
	var c_ *list.Element
	var toRemove *list.Element = nil
	for c_ = cl.Front(); c_ != nil; c_ = c_.Next() {
		c := c_.Value.(*CollectiveCall)
		_, ok := c.callers[info.Rank]
		if c.funcName == info.FunctionName && !ok {
			c.callers[info.Rank] = &info
			if len(c.callers) == len(connections) {
				toRemove = c_
			}
			break
		}
	}

	if toRemove != nil {
		cl.Remove(toRemove)
	}

	if c_ == nil {
		clrs := make(map[int]*utils.CollectiveInfo)
		clrs[info.Rank] = &info
		c := &CollectiveCall{
			info.FunctionName,
			clrs,
		}
		cl.PushBack(c)
	}
}

func pendingCollectiveInfo() (calls []CollectiveCall) {
	collectiveCallList.mux.Lock()
	defer collectiveCallList.mux.Unlock()
	cl := collectiveCallList.calls
	for c_ := cl.Front(); c_ != nil; c_ = c_.Next() {
		c := c_.Value.(*CollectiveCall)
		calls = append(calls, *c)
	}
	return
}

func prettyPrintCollectiveInfo(calls []CollectiveCall, t *tui.TUI) {
	for _, call := range calls {
		s := fmt.Sprintf("Collective function %s:\n", call.funcName)
		for i := 0; i < len(connections); i++ {
			info, ok := call.callers[i]
			if !ok {
				s += fmt.Sprintf("Rank %d: pending\n", i)
			} else {
				s += fmt.Sprintf("Rank %d: Called at %s\n", i, info.LineInfo)
			}
		}
		t.ShowMessagesAll(s)

	}
}
