package tui

import (
	"fmt"
	"log"
	"sort"
	"strconv"
	"sync"

	tui "github.com/marcusolsson/tui-go"
)

// Go has this weird kind of inheritance so I need to create
// these structs which are mainly created to override methods
// in the views in terminal
type BoxWithKeyHandling struct {
	// Box widget with a special function to handle key events
	*tui.Box
	KeyEventFunction func(box *tui.Box)
}

type ScrollableScrollArea struct {
	// ScrollArea widget with a special function to make the area scrollable
	// with arrow keys
	*tui.ScrollArea
	keyToScrollFunction map[string]interface{}
}

type InputWithKeyHandling struct {
	// Input widget with a special function to handle key events
	*tui.Entry
	KeyEventFunction func(entry *tui.Entry, ev *tui.KeyEvent) bool
}

type TUI struct {
	root               *BoxWithKeyHandling
	ui                 tui.UI
	clients            map[int]*tui.Box // All boxes which are used to show info from clients
	Input              *InputWithKeyHandling
	clientParent       *tui.Box
	ranks              []int
	numOfClients       int
	history            map[int][]string
	cmdHistory         []string
	variables          map[int][]VariableInfo
	histPtr            int
	// Shows if an internal window is enabled. Currently, there's only one internal
	// Window, that is Variable window
	isWindowEnabled    map[string]bool
	// Data related to an internal window
	internalWindowData map[string]interface{}
	// Maps the number of a window to the window object itself
	numberToWindow     map[int]Focusable
	// Used to know which number to give to the next created window
	lastWindowNumber   int
}

// Struct to store information about a variable in the running program
type VariableInfo struct {
	Name          string
	VarType       string
	ValueOptional string
}

// Interface to point to windows which can be focused
// TODO: there's a facility for creating focus chains in this TUI library
// Use that to create a focus chain which can be traversed with tab key
type Focusable interface {
	SetFocused(bool bool)
	IsFocused() bool
}

// A mapping of window to the function which knows how to draw that window
var internalWindowPainters = make(map[string]interface{})
var logger *log.Logger
// The maps in Go are not thread safe so always use this mutex before modifying a map
var mutex sync.Mutex

// NewTUI creates a new instance of a TUI
// it initializes the number of clients to be displayed to be as 2
func NewTUI(ranks []int, l *log.Logger) (t *TUI) {
	logger = l
	t = new(TUI)
	t.clients = make(map[int]*tui.Box)
	t.clientParent = tui.NewHBox()
	t.Input = newInputWithKeyHandling(func(entry *tui.Entry, ev *tui.KeyEvent) bool {
		switch ev.Name() {
		case "Up":
			if len(t.cmdHistory) == 0 {
				return false
			}

			if t.histPtr < 0 || t.histPtr > len(t.cmdHistory) {
				panic("History pointer gone")
			}

			if t.histPtr != 0 {
				t.histPtr--
			}
			t.Input.SetText(t.cmdHistory[t.histPtr])
			return true
		case "Down":
			if len(t.cmdHistory) == 0 || t.histPtr == len(t.cmdHistory) {
				return false
			}

			if t.histPtr < 0 || t.histPtr > len(t.cmdHistory) {
				panic("History pointer gone")
			}

			t.histPtr++
			if t.histPtr == len(t.cmdHistory) {
				t.Input.SetText("")
				return false
			}
			t.Input.SetText(t.cmdHistory[t.histPtr])
			return true
		}
		return false
	})
	t.Input.SetFocused(true)
	t.Input.SetSizePolicy(tui.Expanding, tui.Maximum)
	t.root = newBoxWithKeyHandling(func(box *tui.Box) {
		t.Input.SetFocused(true)
	})
	t.ranks = ranks
	t.numOfClients = 2
	t.history = make(map[int][]string)
	t.histPtr = 0
	t.variables = make(map[int][]VariableInfo)
	t.isWindowEnabled = make(map[string]bool)
	t.internalWindowData = make(map[string]interface{})
	t.numberToWindow = make(map[int]Focusable)
	t.lastWindowNumber = 0
	addInternalWindows(t)
	return
}

func addInternalWindows(t *TUI) {
	// The function which knows how to draw a variable window
	// The prototype for a WindowPainter is fixed, it is
	// func(t *TUI) tui.Widget
	// TODO: Should make an interface out of this and make all WindowPainters
	// inherit from that interface
	variableWindowPainter := func(t *TUI) tui.Widget {
		rank, _ := strconv.Atoi(fmt.Sprintf("%v", t.internalWindowData["rank"]))
		variableInfo := t.variables[rank]

		box := tui.NewVBox()

		for _, variable := range variableInfo {
			variableInfoBox := tui.NewHBox(
				tui.NewPadder(1, 0, tui.NewLabel(variable.toString())),
				tui.NewSpacer(),
			)
			box.Append(variableInfoBox)
		}

		windowNumber := t.GetNextWindowNumber()

		scroller := newScrollableScrollArea(box)
		//scroller.SetAutoscrollToBottom(true)
		scrollerBox := tui.NewVBox(scroller)
		scrollerBox.SetBorder(true)
		scrollerBox.SetTitle(fmt.Sprintf("Variables at rank %d (%d)", rank, windowNumber))
		addScrollingKeyBindings(t, scroller)
		t.numberToWindow[windowNumber] = scroller
		return scrollerBox
	}
	t.isWindowEnabled["vars"] = false
	internalWindowPainters["vars"] = variableWindowPainter
}

func (t *TUI) EnableInternalWindow(name string, data ...interface{}) {
	switch name {
	case "vars":
		rank, _ := strconv.Atoi(fmt.Sprintf("%v", data[0]))
		t.internalWindowData["rank"] = rank
		t.isWindowEnabled["vars"] = true
		t.reDraw(0, "")
	}
}

func (t *TUI) DisableInternalWindow(name string) {
	switch name {
	case "vars":
		t.isWindowEnabled["vars"] = false
		t.reDraw(0, "")
	}
}

func (t *TUI) IsInternalWindowEnabled(name string) bool {
	return t.isWindowEnabled[name]
}

func (t *TUI) ReDraw() {
	t.reDraw(0, "")
}

func (t *TUI) AddVariables(rank int, variables []VariableInfo) {
	mutex.Lock()
	defer mutex.Unlock()
	t.variables[rank] = variables
}

func (t *TUI) GetVariables(rank int) ([]VariableInfo, bool) {
	varInfo, ok := t.variables[rank]
	return varInfo, ok
}

func (t *TUI) drawClient(title string, rank int) *tui.Box {
	box := tui.NewVBox()

	// if history exists
	for _, hist := range t.history[rank] {
		histBox := tui.NewHBox(
			tui.NewPadder(1, 0, tui.NewLabel(hist)),
			tui.NewSpacer(),
		)
		box.Append(histBox)
	}

	windowNumber := t.GetNextWindowNumber()

	scroller := newScrollableScrollArea(box)
	//scroller.SetAutoscrollToBottom(true)
	scrollerBox := tui.NewVBox(scroller)
	scrollerBox.SetBorder(true)
	scrollerBox.SetTitle(fmt.Sprintf("%s (%d)", title, windowNumber))
	t.clients[rank] = box
	addScrollingKeyBindings(t, scroller)
	t.numberToWindow[windowNumber] = scroller
	return scrollerBox
}

// drawInput draws the input textarea where the user
// can type their command which passes over to the server
func (t *TUI) drawInput() {
	inputBox := tui.NewHBox(t.Input)
	inputBox.SetBorder(true)
	inputBox.SetSizePolicy(tui.Expanding, tui.Maximum)
	t.root.Append(t.Input)
}

// DrawUI paints the complete UI along with the clients and inputBox
func (t *TUI) DrawUI() {
	for i := 0; i < t.numOfClients; i++ {
		box := t.drawClient(fmt.Sprintf("rank-%d", i), i)
		t.clientParent.Append(box)
	}
	t.root.Append(t.clientParent)
	t.drawInput()
	var err error
	t.ui, err = tui.New(t.root)
	if err != nil {
		panic(err)
	}

	// If the focus is on another window, for example to scroll, pressing
	// escape will bring focus back to the input prompt
	t.ui.SetKeybinding("Esc", func() {
		for _, f := range t.numberToWindow {
			if f.IsFocused() {
				f.SetFocused(false)
				break
			}
		}

		t.Input.SetFocused(true)
	})

	go func() {
		err := t.ui.Run()
		if err != nil {
			panic(err)
		}
	}()
}

func (t *TUI) AddToCmdHistory(hist string) {
	t.cmdHistory = append(t.cmdHistory, hist)
	t.histPtr = len(t.cmdHistory)
}

func (t *TUI) reDraw(rank int, cat string) {
	t.lastWindowNumber = 0
	var currClients []int
	for r := range t.clients {
		currClients = append(currClients, r)
	}
	t.clients = make(map[int]*tui.Box)
	for t.clientParent.Length() != 0 {
		t.clientParent.Remove(0)
	}

	for k, v := range t.isWindowEnabled {
		if !v {
			continue
		} else {
			switch k {
			case "vars":
				t.clientParent.Append(internalWindowPainters["vars"].(func(tuiArg *TUI) tui.Widget)(t))
			}
		}
	}

	if cat == "Add" {
		currClients = append(currClients, rank)
	} else if cat == "Remove" {
		i := -1
		for idx, r := range currClients {
			if r == rank {
				i = idx
				break
			}
		}
		if i != -1 {
			currClients = append(currClients[:i], currClients[i+1:]...)
		}
	}
	t.numOfClients = len(currClients)
	sort.Ints(currClients)
	for _, i := range currClients {
		box := t.drawClient(fmt.Sprintf("rank-%d", i), i)
		t.clientParent.Append(box)
	}

}

func (t *TUI) Add(rank int) {
	// if the box exists dont redraw
	if _, ok := t.clients[rank]; ok {
		return
	}
	t.reDraw(rank, "Add")
}

func (t *TUI) Remove(rank int) {
	// if the box dont exists dont redraw
	if _, ok := t.clients[rank]; !ok {
		return
	}
	t.reDraw(rank, "Remove")
}

func (t *TUI) Swap(newRank int, oldRank int) {
	t.Remove(oldRank)
	t.Add(newRank)
}

// ShowMessagesAll displays a particular message
// to all the clients in display
func (t *TUI) ShowMessagesAll(message string) {
	t.ui.Update(func() {
		command := tui.NewHBox(
			tui.NewPadder(1, 0, tui.NewLabel(message)),
			tui.NewSpacer(),
		)

		var rank int
		for rank = range t.ranks {
			t.history[rank] = append(t.history[rank], message)
		}

		var b *tui.Box
		for rank, b = range t.clients {
			b.Append(command)
		}
	})
}

// ShowUserInputAll displays user input
// to all the clients in display
func (t *TUI) showUserInputAll(message string) {
	command := tui.NewHBox(
		tui.NewPadder(1, 0, tui.NewLabel(message)),
		tui.NewSpacer(),
	)

	var rank int
	for rank = range t.ranks {
		t.history[rank] = append(t.history[rank], message)
	}

	var b *tui.Box
	for rank, b = range t.clients {
		b.Append(command)
	}
}

// ShowInputClients sows the messages of particular client(s)
func (t *TUI) ShowUserInputClients(message string, ranks []int) {
	if len(ranks) == 0 {
		t.showUserInputAll(message)
		return
	}

	for _, rank := range ranks {
		command := tui.NewHBox(
			tui.NewPadder(1, 0, tui.NewLabel(message)),
			tui.NewSpacer(),
		)

		t.history[rank] = append(t.history[rank], message)

		var c *tui.Box
		var ok bool
		if c, ok = t.clients[rank]; !ok {
			continue
		}
		c.Append(command)
	}
}

// The UI drawing system is single threaded. So whenever you want
// to modify UI from another thread, for example some new data came
// from a client, you must create a lambda to update the UI and then
// pass it to this function so that the UI gets updated properly
func (t *TUI) RunOnUiThread(f func()) {
	t.ui.Update(f)
}

// ShowMessagesClient the messages of a particular client
// If the specific client is not in display
// the function return void silently
func (t *TUI) ShowMessagesClient(message string, rank int) {
	t.ui.Update(func() {
		command := tui.NewHBox(
			tui.NewPadder(1, 0, tui.NewLabel(message)),
			tui.NewSpacer(),
		)

		t.history[rank] = append(t.history[rank], message)

		var c *tui.Box
		var ok bool
		if c, ok = t.clients[rank]; !ok {
			return
		}
		c.Append(command)
	})

}

func (t *TUI) ChangeFocusedWindow(windowNumber int) {
	t.Input.SetFocused(false)
	if focusable, ok := t.numberToWindow[windowNumber]; ok {
		focusable.SetFocused(true)
	}
}

func (t *TUI) GetNextWindowNumber() int {
	t.lastWindowNumber += 1
	return t.lastWindowNumber
}

func (t *TUI) Quit() {
	t.ui.Quit()
}

func (v *VariableInfo) toString() string {
	var variableValue string
	if len(v.ValueOptional) == 0 {
		variableValue = ""
	} else {
		variableValue = " = " + v.ValueOptional
	}
	return fmt.Sprintf("%s %s%s",
		v.VarType, v.Name, variableValue)
}

func newBoxWithKeyHandling(fn func(box *tui.Box)) *BoxWithKeyHandling {
	return &BoxWithKeyHandling{
		Box:              tui.NewVBox(),
		KeyEventFunction: fn,
	}
}

func newScrollableScrollArea(widget tui.Widget) *ScrollableScrollArea {
	return &ScrollableScrollArea{
		ScrollArea:          tui.NewScrollArea(widget),
		keyToScrollFunction: make(map[string]interface{}),
	}
}

func newInputWithKeyHandling(fn func(entry *tui.Entry, ev *tui.KeyEvent) bool) *InputWithKeyHandling {
	return &InputWithKeyHandling{
		Entry:            tui.NewEntry(),
		KeyEventFunction: fn,
	}
}

// Set key bindings for a client window for scrolling
func addScrollingKeyBindings(t *TUI, scroller *ScrollableScrollArea) {
	scroller.keyToScrollFunction["Up"] = func(s *ScrollableScrollArea) {
		s.Scroll(0, -1)
		t.ui.Repaint()
	}
	scroller.keyToScrollFunction["Down"] = func(s *ScrollableScrollArea) {
		s.Scroll(0, 1)
		t.ui.Repaint()
	}
	scroller.keyToScrollFunction["Left"] = func(s *ScrollableScrollArea) {
		s.Scroll(-1, 0)
		t.ui.Repaint()
	}
	scroller.keyToScrollFunction["Right"] = func(s *ScrollableScrollArea) {
		s.Scroll(1, 0)
		t.ui.Repaint()
	}
	scroller.keyToScrollFunction["t"] = func(s *ScrollableScrollArea) {
		s.ScrollToTop()
		t.ui.Repaint()
	}
	scroller.keyToScrollFunction["b"] = func(s *ScrollableScrollArea) {
		s.ScrollToBottom()
		t.ui.Repaint()
	}
}

func (b *BoxWithKeyHandling) OnKeyEvent(ev tui.KeyEvent) {
	if b.IsFocused() && ev.Key == tui.KeyEsc {
		b.KeyEventFunction(b.Box)
	} else {
		b.Box.OnKeyEvent(ev)
	}
}

func (s *ScrollableScrollArea) OnKeyEvent(ev tui.KeyEvent) {
	if !s.IsFocused() {
		s.ScrollArea.OnKeyEvent(ev)
		return
	}

	if function, ok := s.keyToScrollFunction[ev.Name()]; ok {
		function.(func(area *ScrollableScrollArea))(s)
	}
}

func (e *InputWithKeyHandling) OnKeyEvent(ev tui.KeyEvent) {
	if !e.IsFocused() {
		e.Entry.OnKeyEvent(ev)
		return
	}

	if !e.KeyEventFunction(e.Entry, &ev) {
		e.Entry.OnKeyEvent(ev)
	}
}
