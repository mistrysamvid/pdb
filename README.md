# Prerequisites

To install and run our code, you need the following:
+ mpicc (we used 3.3)
+ gcc (we used 8.3)
+ go (we used 1.11.6. At least 1.11 is needed)

To build the server, `cd` into `pd-server` and run:

```sh
$ go build
```

Go will take care of the rest of the dependencies (downloading them over the internet if needed).

To build the client, `cd` into `pd-client` and run:

```sh
$ make
```

Make sure that you have `PATH` and `LD_LIBRARY_PATH` set up to point to the necessary MPI files. In case you
are running `pd-client` outside the `pd-client` directory, you also need to specify another environment
variable, `PD_FILE_DIR`, which must also be set to the path of the directory containing the `mpic.so`
shared object (this is the same directory which contains `pd-client`).

# Running

In a terminal window, run

```sh
$ cd pd-server
$ ./pd-server 2> /dev/null
```

In a separate terminal window, run

```sh
$ cd pd-client
$ mpiexec -n 4 ./pd-client "<your-ip>:8080" ./your_binary
```

For ease of testing, there is a preexisting C file which is compiled into a binary, so for testing purposes, you can simply run

```sh
$ cd pd-client
$ mpiexec -n 4 ./pd-client "localhost:8080" ./test
```

# Available Commands

The TUI has two main components. First is a set of _i_ panes, each of
which denotes output at the rank _i_. Below that is an input in which you
can type commands.

A command history is also maintained, so you can use up arrow and down arrow
keys to access previously accessed commands.

After the TUI is open, the following commands are available.

```sh
# Quit the TUI
quit

# Remove rank pane
remove <rank>

# Add rank pane
add <rank>

# Swap rank N pane, add rank M pane
swap M N

# Execute gdb command "break main" at all processes
break main

# Execute gdb command "break main" at rank 1,2,3,5,6,7 only
pdb_break main [r=1..4,5..8]

# Track/untrack collective "MPI_Barrier"
pdb_trackcoll MPI_Barrier

# List all pending collectives with who called info
pdb_listcoll
```

Some additional points to be remembered while running the debugger:

- Usually, debugging is started by the `run` or `r` command in `gdb`. When there
  is further stoppage, you can use `continue` or `c`. However, in our debugger,
  you need to start debugging **straight from the `continue` or `c`** command. This
  is because the `run` command is used internally to set up networking with the server.
- The debugger does not assume responsibility for anything before `MPI_Init` and after
  `MPI_Finalize`. Hence, setting breakpoint on main is not that useful. You are dropped
  into the `pdb` prompt after `MPI_Init` is complete.
- Programs to be debugged need to be compiled with `-g` option. While this option _should_
  be enabled even for normal (non-parallel) `gdb` usage, we actually can't work without this option if
  you are tracking collectives.
