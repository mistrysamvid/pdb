#include <stdio.h>
#include <iostream>
#include <mpi.h>

int main(int argc, char *argv[]){

    int rank;
    int numProcesses;
    int destinationProcess;
    int sourceProcess;
    int sendInt;
    int receiveInt;
    MPI_Status status;

    const int DEFAULT_INT = -9;

    /*Start MPI environment */
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &numProcesses);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    /* Abort if not exactly 2 processes */
    if(numProcesses != 2){
        std::cout << "This application is meant to be run with 2 processes." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
    }

    /* Establish sending and receiving ranks */
    sendInt = rank;
    receiveInt = DEFAULT_INT;
    if(rank == 0){
        destinationProcess = 1;
        sourceProcess = rank;
    } else {
        destinationProcess = 0;
        sourceProcess = rank;
    }

    /* Display a message containing relevant info prior to MPI_Sendrecv call*/
    std::cout << "My rank is: " << rank << std::endl;
    std::cout << "My receiveInt is: " << receiveInt << std::endl;
    std::cout << "My sendInt is:" << sendInt << std::endl;
    std::cout << "Carrying out MPI_Sendrecv" << std::endl;

    MPI_Sendrecv(&sendInt, 1, MPI_INT, destinationProcess, 0 , &receiveInt, 1, MPI_INT, destinationProcess, 0, MPI_COMM_WORLD, &status);

    /* Display a message containing relevant info prior to MPI_Sendrecv call*/
    std::cout <<" MPI_Sendrecv carried out." << std::endl;
    std::cout << "My receiveInt is: " << receiveInt << std::endl;
    std::cout << "My sendInt is:" << sendInt << std::endl;


    MPI_Finalize();
}
