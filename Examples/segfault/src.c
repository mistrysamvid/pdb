#include <iostream>
#include <stdio.h>
#include <mpi.h>

// k = 3, N = 12,  1,2,3, 4,5,6, 7,8,9, 10,11,12

int main(int argc, char **argv) {
    int N = 3, size, myrank;
    int k = 10;
    // std::cin >> N;
    // std::cin >> k;
    int *mass = new int[N];
    int *recv = new int[k];

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);

    if (myrank == 0) {
        std::cout << "get k and n \n";

        for (int i = 0; i < N; ++i) {
            mass[i] = i;
            std::cout << i << " written\n";
        }
    }

    MPI_Scatter(mass, k, MPI_INT, recv, k, MPI_INT, 0, MPI_COMM_WORLD);

    int sum = 0;
    std::cout << "myrank" << myrank << '\n';
    for (int i = 0; i < k; ++i) {
        std::cout << recv[i] << '\n';
    }

    MPI_Finalize();

    return 0;
}
