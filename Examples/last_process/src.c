#include <stdio.h>
#include "mpi.h"
#include <stdlib.h>

int main(int argc, char **argv) {
    int send_data = 100;
    int recv_data = -100;
    int rank = -1, w_size = -1;
    int *nullptr = NULL;
    MPI_Request request;
    MPI_Status status;

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &w_size);

    int prev = (rank - 1 < 0) ? w_size - 1 : rank - 1;
    int next = (rank + 1 == w_size) ? 0 : rank + 1;

    MPI_Isend(&send_data, (prev == w_size - 1) ? 1 : 1, MPI_INT, prev, 99, MPI_COMM_WORLD, &request);
    MPI_Recv(&recv_data, 1, MPI_INT, next, 99, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    MPI_Wait(&request, &status);

    free(nullptr);
    printf("Freed null pointer\n");

    MPI_Finalize();
    return 0;
}
