#include <iostream>
#include "mpi.h"

// void merge(int*, int*, int, int, int);
// void mergeSort(int*, int*, int, int);

int main(int argc, char** argv) {

    


    int world_rank;
    int world_size;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    int n = 5;
    int* original_array{ new int[n] {} };
    //int original_array[]=new int[n];

    int c;
    srand(time(NULL));

    if (world_rank == 0) {
        printf("This is the unsorted array: ");
        for (c = 0; c < n; c++) {

        original_array[c] = rand() % n;
        printf("%d ", original_array[c]);

        }
        printf("\n");
        printf("\n");
    }
    
    
    int size = n / world_size;
    int* sub_array=NULL;
    int* tmp_array = NULL;
    int* sorted = NULL;

    if (world_rank == 0) {

        sorted = { new int[n] {} };

    }


    if ((world_rank == 1) && (n % world_size != 0)) {
        int r = n % world_size;
        int size2 = size + r;
        sub_array = { new int[size2] {} };
        MPI_Scatter(original_array, size2, MPI_INT, sub_array, size2, MPI_INT, 0, MPI_COMM_WORLD);
        tmp_array = { new int[size2] {} };
        // mergeSort(sub_array, tmp_array, 0, (size2 - 1));
        MPI_Gather(sub_array, size2, MPI_INT, sorted, size2, MPI_INT, 0, MPI_COMM_WORLD);
        printf("Process %d here\n", world_rank);
    }
    else {
        sub_array = { new int[size] {} };
        MPI_Scatter(original_array, size, MPI_INT, sub_array, size, MPI_INT, 0, MPI_COMM_WORLD);
        tmp_array = { new int[size] {} };
        // mergeSort(sub_array, tmp_array, 0, (size - 1));
        MPI_Gather(sub_array, size, MPI_INT, sorted, size, MPI_INT, 0, MPI_COMM_WORLD);
        printf("Process %d in else\n", world_rank);
    }

    

    

    
    // if (world_rank == 0) {

    //     printf("Array state before final mergeSort call: ");
    //     for (c = 0; c < n; c++) {

    //         printf("%d ", sorted[c]);

    //     }
        
    //     printf("\n");

    //     int* other_array{ new int[n] {} };
    //     // mergeSort(sorted, other_array, 0, (n - 1));

    //     printf("This is the sorted array: ");
    //     for (c = 0; c < n; c++) {

    //         printf("%d ", sorted[c]);

    //     }

    //     printf("\n");
    //     printf("\n");

    //     delete[] sorted;
    //     delete[] other_array;

    // }

    // delete[] original_array;
    // delete[] sub_array;
    // delete[] tmp_array;

    // /********** Finalize MPI **********/
    // MPI_Finalize();

}
