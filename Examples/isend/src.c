#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"

int main( int argc, char* argv[] ) {
    MPI_Init(&argc, &argv);
    int rank, p;
    MPI_Comm_rank( MPI_COMM_WORLD, &rank );
    MPI_Comm_size( MPI_COMM_WORLD, &p );

    int a,b, i, j;
    int maxNumber = 8;

    // buggy version, overwrites a region which is still in nonblocking transfer
    int(*tab)[maxNumber] = malloc(sizeof(int[maxNumber + 1][maxNumber + 1]));
    // correct version
    /* int(*tab)[maxNumber + 1] = malloc(sizeof(int[maxNumber + 1][maxNumber + 1])); */
    // MPI_Request* r = malloc(sizeof(MPI_Request));

    if(rank == 0){
        for(i = 0; i < maxNumber + 1; i++){
            for(j = 0; j < maxNumber + 1; j++){
                tab[i][j] = 2*i+i*j;
            }
            MPI_Request r[1024];
            for(a = 1; a < p; a++){
                MPI_Isend(&tab[i], maxNumber + 1, MPI_INT, a, i, MPI_COMM_WORLD, &r[a - 1]);
            }
            MPI_Waitall(p - 1, r, MPI_STATUSES_IGNORE);
        }
    }
    else{
        for(i = 1; i < p; i++){
            if(rank == i){
                for(j = 0; j < maxNumber + 1; j++){
                    printf("Process %d wait the block %d to process 0\n", i, j);
                    MPI_Recv(&tab[j], maxNumber + 1, MPI_INT, 0, j, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                    printf("Process %d receive the block %d to process 0\n", i, j);
                }
            }           

        }
    }

    MPI_Finalize();

    return 0;
}
