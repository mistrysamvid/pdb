module pd-client

go 1.14

require pd-utils v0.0.0-00010101000000-000000000000

replace pd-server => ../pd-server

replace pd-client => ../pd-client

replace pd-utils => ../utils
