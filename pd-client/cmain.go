package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"log"
	"math"
	"net"
	"os"
	"strconv"
	"strings"

	"pd-utils"
)

var a bool = true
var portLimit = 4096

func main() {
	if len(os.Args) < 3 {
		fmt.Printf("Usage %s (<hostname>:<port>)+ <filename>\n", os.Args[0])
		os.Exit(1)
	}

	/*for a {
		continue
	}*/
	a = false

	// The debugger can listen on multiple ports
	// The ports should be specified along with IPs
	// and should be continuous in the command line
	// arguments
	i := 1
	for ; i < len(os.Args); i++ {
		if !strings.Contains(os.Args[i], ":") {
			i++
			break
		}
	}
	numPorts := i - 2
	ports := make([]string, numPorts)
	for i = 1; i < 1 + numPorts; i++ {
		ports[i - 1] = os.Args[i]
	}

	filename := os.Args[i]

	// Channel for providing information about collective calls
	cInfoChan := make(chan utils.CollectiveInfo)
	// Channel for receiving internal debugger information from server
	internalInfoChan := make(chan map[string]interface{})
	gdbInstance := utils.NewGdb(cInfoChan, internalInfoChan)

	pdFilename := gdbInstance.InitGdb(filename, strings.Join(os.Args[3:], " "))

	// Every MPI client process writes its rank and size of the world in a
	// temporary file whose name is present in pdFilename variable
	f, err := os.Open(pdFilename)
	utils.CheckError(err)
	line, err := bufio.NewReader(f).ReadString('\n')
	utils.CheckError(err)

	rankAndSize := strings.Split(line, ",")
	rank, _ := strconv.Atoi(strings.TrimSpace(rankAndSize[0]))
	size, _ := strconv.Atoi(strings.TrimSpace(rankAndSize[1]))

	if math.Ceil(float64(size) / float64(portLimit)) > float64(numPorts) {
		panic("Number of ports not enough to accommodate all ranks")
	}

	myPortIndex := rank / portLimit

	conn, err := net.Dial("tcp", ports[myPortIndex])
	log.Printf("Rank %d Dialing at %s", rank, ports[myPortIndex])
	utils.CheckError(err)
	fmt.Fprintf(conn, "%s", line)
	log.Printf("GDB Initialized\n")

	// Each time we get some communicator info, send to the server to process.
	go (func() {
		var c utils.CollectiveInfo
		for {
			c = <-cInfoChan
			out, err := json.Marshal(c)
			if err != nil {
				continue
			}
			fmt.Fprintf(conn, "COLLECTIVE:%s\n", out)
		}
	})()

	go (func() {
		var internalInfoMap map[string]interface{}
		for {
			internalInfoMap = <-internalInfoChan
			switch internalInfoMap["KEY"] {
			case "FRAMEVARS":
				// We're sending the information about variables to the server
				// to show in the variable window
				array := internalInfoMap["VALUE"]
				varsWithRank := make(map[string]interface{})
				varsWithRank["RANK"] = internalInfoMap["RANK"]
				varsWithRank["VALUE"] = array
				if fName, ok := internalInfoMap["FUNC"]; ok {
					varsWithRank["FUNC"] = fName
				}
				out, err := json.Marshal(varsWithRank)
				if err != nil {
					println("Error encountered while sending variable info")
					continue
				}
				fmt.Fprintf(conn, "FRAMEVARS:%s\n", out)
			case "EXITED":
				// This client process has finished its execution, notify the server about that
				exitInfo := make(map[string]interface{})
				exitInfo["RANK"] = internalInfoMap["RANK"]

				out, err := json.Marshal(exitInfo)
				if err != nil {
					println("Error encountered while sending exit info")
					continue
				}
				fmt.Fprintf(conn, "EXITINFO:%s\n", out)
			}
		}
	})()

	// Each output that the gdb instance gets from gdb mi must be processed.
	// One hook is added here, which will send all ~console messages to the server.
	gdbInstance.AddNotificationHook("ConsoleSendingHook", func(notification map[string]interface{}) bool {
		if notification["type"] == "console" {
			// On getting a console notification, relay it to the server.
			// Filter newlines though. They will be added by us at server side!
			payload := strings.TrimSpace(notification["payload"].(string))
			if payload == "" || payload == "\n" {
				return true
			}
			fmt.Fprintf(conn, "CONSOLE:%s\n", payload)
			//fmt.Println(payload)
		}
		return true
	})

	gdbInstance.AddNotificationHook("ErrorSendingHook", func(notification map[string]interface{}) bool {
		if notification["class"] == "error" {
			// On getting a error notification, tell the server.
			payload := notification["payload"].(map[string]interface{})
			msg := payload["msg"].(string)
			msg = strings.TrimSpace(msg)
			fmt.Fprintf(conn, "ERROR:%s\n", msg)
			//fmt.Println(msg)
		}
		return true
	})

	gdbInstance.AddNotificationHook("LoggingHook", func(notification map[string]interface{}) bool {
		//jsonStr, _ := json.Marshal(notification)
		//log.Println(string(jsonStr))
		return true
	})

	// Each message from the server needs to be processed using ProcessMessage.
	processCommandsDone := make(chan bool)
	go gdbInstance.ProcessCommands(conn, processCommandsDone)
	<-processCommandsDone
}
