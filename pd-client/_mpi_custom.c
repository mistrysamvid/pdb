#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#define _GENERATE_INTERNAL_METHOD(mname) \
  int internal_##mname() {               \
    __x = 0;                             \
    return 0;                            \
  }

#define _GENERATE_EXT_METHOD(mname, typeargs, args) \
  int mname typeargs {                              \
  int rank = -1;                                    \
  MPI_Comm_rank(comm, &rank);                       \
  __x = rank;                                       \
  internal_##mname();                               \
  volatile int result = P##mname args;              \
  return result;                                    \
  }

static volatile int __x;

/* These functions are used internally so no functions with these names
*  can be present in the user program.
*/
MPI_Request* PDB_Request_start(const void *, MPI_Datatype, int, MPI_Request*);
MPI_Request* PDB_Request_end(MPI_Request*);

// Line number sensitive body
// Be very careful while modifying
// Line number sensitive means the debugger is dependent upon the order of lines
// inside this function. So adding, removing or reordering the lines may break
// the debugger. Almost all of the dependencies can be found in InitGdb function, but
// there may be other dependencies.
int MPI_Init(int *argc, char ***argv) {
  void error_handler(MPI_Comm*, int*, ...);
  int return_code, size, rank;
  MPI_Errhandler err_handler;
  FILE* f;
  char *filename;

  printf("Preloaded.\n");
  return_code = PMPI_Init(argc, argv);
  if (return_code != MPI_SUCCESS) {
    return return_code;
  }
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  filename = getenv("FILENAME");
  f = fopen(filename, "w");
  fprintf(f, "%d,%d\n", rank, size);
  fclose(f);
  MPI_Comm_create_errhandler(error_handler, &err_handler);
  MPI_Comm_set_errhandler(MPI_COMM_WORLD, err_handler);
  return return_code;
}

// Line number sensitive body
// Be very careful before modifying
void error_handler(MPI_Comm *comm, int *return_code, ...) {
    if (*return_code == MPI_SUCCESS) {
        return;
    } else if (*return_code == MPI_ERR_COMM) {
        printf("Invalid communicator");
        MPI_Abort(*comm, *return_code);
    } else if (*return_code == MPI_ERR_ARG) {
        printf("Invalid arguments to MPI_Error_string");
        MPI_Abort(*comm, *return_code);
    } else {
        char *error_string = (char*) malloc(MPI_MAX_ERROR_STRING * sizeof(char));
        int str_len = 0;
        MPI_Error_string(*return_code, error_string, &str_len);
        printf("%s\n", error_string);
        MPI_Abort(*comm, *return_code);
    }
}

int MPI_Isend(const void *buf, int count, MPI_Datatype datatype, int dest, int tag,
              MPI_Comm comm, MPI_Request *request) {
    int code = PMPI_Isend(buf, count, datatype, dest, tag, comm, request);
    PDB_Request_start(buf, datatype, count, request);
    return code;
}

int MPI_Wait(MPI_Request *request, MPI_Status *status) {
    PDB_Request_end(request);
    int code = PMPI_Wait(request, status);
    return code;
}

// There *must* be at least one line in this function
// Parameter names are hardcoded in the debugger too, so be extra careful while changing
// the name of variables. However, the order of parameters is not important.
// Line number sensitive body, be careful while modifying
// Preferably, leave the first 2 lines as they are
MPI_Request* PDB_Request_start(const void *buf, MPI_Datatype datatype, int count, MPI_Request *request) {
    int size;
    MPI_Type_size(datatype, &size);
    return NULL;
}

// Line number sensitive body
// Be very careful while modifying
// Parameter names are hardcoded in the debugger too, so be extra careful while changing
// the name of variables.
MPI_Request* PDB_Request_end(MPI_Request *request) {
    size_t sz = snprintf(NULL, 0, "%p", request);
    char *buf = (char*) malloc(sizeof(char) * (sz + 1));
    snprintf(buf, sz + 1, "%p", request);
    return NULL;
}

_GENERATE_INTERNAL_METHOD(MPI_Barrier);
_GENERATE_EXT_METHOD(MPI_Barrier,(MPI_Comm comm), (comm));

_GENERATE_INTERNAL_METHOD(MPI_Bcast);
_GENERATE_EXT_METHOD(MPI_Bcast,
                     (void* data, int count, MPI_Datatype datatype, int root, MPI_Comm comm),
                     (data, count, datatype, root, comm));

/* int MPI_Barrier(MPI_Comm comm) { */
/*   int rank = -1; */
/*   MPI_Comm_rank(comm, &rank); */
/*   __x = rank; // prevent optimizing away */
/*   internal_MPI_Barrier(); */
/*   volatile int result = PMPI_Barrier(comm); */
/*   return result; */
/* } */
