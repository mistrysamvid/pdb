package utils

import (
	"bufio"
	"encoding/json"
	"regexp"
	"sync"

	// "encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"strings"

	"github.com/milindl/gdb"
)

const MPI_COMM_WORLD int = 1140850688

type GdbInstance struct {
	breakpointHitNotification  chan int // channel to send info when breakpoint is hit
	pdFilename                string // File in which rank and size data to write
	internal                  *gdb.Gdb // Actual GDB object to which we can send commands
	hooks                     map[string]func(notification map[string]interface{}) bool
	cInfoChan                 chan CollectiveInfo
	internalInfoChan          chan map[string]interface{}
	trackedCollectives        map[string]bool
	rank                      int
	// Breakpoints which track a memory region for modification, while it is under
	// nonblocking transfer
	hardwareBreakpoints       map[string]int
	bufferToRequest           map[string]string
	// Internal configuration information about the debugger
	config                     map[string]interface{}
}

type CollectiveInfo struct {
	Rank         int
	LineInfo     string
	FunctionName string
}

const (
	StopNonblockingModification  = "stopNonblockingModificationMessages"
	StartNonblockingModification = "startNonblockingModificationMessages"
	NonblockingModification      = "NonblockingModification"
)

var breakpointStartLocation = regexp.MustCompile("\\*\\(void\\(\\*\\)\\[(?P<Size>[0-9]+)]\\)\\((?P<Loc>0x[0-9a-fA-F]+)\\)")

var mapWriteMux sync.Mutex

// NewGdb creates a new GdbInstance struct.
func NewGdb(cInfoChan chan CollectiveInfo, infoChan chan map[string]interface{}) (g *GdbInstance) {
	// start a new instance and pipe the target output to stdout
	g = new(GdbInstance)
	g.hooks = make(map[string]func(notification map[string]interface{}) bool)
	g.breakpointHitNotification = make(chan int)
	g.internal, _ = gdb.New(g.handleNotifications)
	g.cInfoChan = cInfoChan
	g.trackedCollectives = make(map[string]bool)
	g.internalInfoChan = infoChan
	g.hardwareBreakpoints = make(map[string]int)
	// This is just an intermediate map which is used to
	// map the request to the starting location of the buffer for
	// that request, so that I can identify the request in the notification
	// from the GDB when the breakpoint is created.
	// Essentially, it's an intermediate assignment for eventual request->breakpoint mapping
	g.bufferToRequest = make(map[string]string)
	// This is a map to store configuration data which can be used to govern the behavior
	// of the debugger
	g.config = make(map[string]interface{})
	g.InitDefaultConfig()
	return
}

// InitGdb does the following:
// 1. Mirror stdout of the target program to stdout of the go program as well as the server. (TODO: Echo output of target to server)
// 2. Add LD_PRELOAD with the shared library file.
// 3. Run the code up till MPI_Init, and write data about rank, size to pdFilename.
func (g *GdbInstance) InitGdb(debugTarget string, arguments string) (pdFilename string) {
	go io.Copy(os.Stdout, g.internal)

	fname := getSoFilepath()
	g.pdFilename = fmt.Sprintf("/tmp/pd_init_data_%d", os.Getpid())

	g.SynchronizedSend("set breakpoint pending on")
	g.SynchronizedSend("file", debugTarget)
	g.SynchronizedSend(fmt.Sprintf("set exec-wrapper env 'LD_PRELOAD=%s' 'FILENAME=%s'", fname, g.pdFilename))
	g.SynchronizedSend("break PMPI_Init")
	// Stop if error_handler's return code is not 0, i.e. there's an error
	g.SynchronizedSend("break error_handler if (*return_code != 0)")
	// Integration with AddressSanitizer, break when sanitizer thinks we're writing
	// on a memory location which we don't own
	g.SynchronizedSend("break __sanitizer::Die")
	// Internal breakpoints to track when a nonblocking communication started and ended
	g.SynchronizedSend("break PDB_Request_start")
	g.SynchronizedSend("break PDB_Request_end")
	g.SynchronizedSend("break PMPI_Finalize")
	g.SynchronizedSend("run " + arguments)
	g.SynchronizedSend("next")
	g.SynchronizedSend("next")
	g.SynchronizedSend("next")
	g.SynchronizedSend("next")

	varInfo := g.getVariableInfo()
	_, err := json.MarshalIndent(varInfo, "", "  ")
	// println(string(str))
	if err != nil {
		panic(err)
	}
	array := varInfo["VALUE"].([]interface{})
	for _, c := range array {
		variableInfoStructure := c.(map[string]interface{})
		if fmt.Sprintf("%v", variableInfoStructure["name"]) == "rank" {
			g.rank, _ = strconv.Atoi(fmt.Sprintf("%v", variableInfoStructure["value"]))
			break
		}
	}

	g.SynchronizedSend("finish")
	g.SynchronizedSend("clear PMPI_Init")

	//g.toggleCollectiveTracking("MPI_Bcast")
	// g.toggleCollectiveTracking("MPI_Barrier")
	// g.SynchronizedSend("break internal_MPI_Barrier")
	// g.SynchronizedSend("break internal_MPI_Bcast")

	return g.pdFilename
}

// This will run indefinitely and process messages from some Reader.
// This reader will (usually) be the Conn of the server.
// For each message, it either runs it in the gdb instance (if the message prefix is RUN:)
// else it prints the message (if the prefix is COMMAND:)
func (g *GdbInstance) ProcessCommands(r io.Reader, processCommandsDone chan bool) {
	scanner := bufio.NewScanner(r)

	for scanner.Scan() {
		CheckError(scanner.Err())
		line := scanner.Text()
		lineSplit := strings.SplitN(line, ":", 2)

		if len(lineSplit) != 2 {
			continue
		}

		if lineSplit[0] == "COMMAND" {
			fmt.Printf("Server message: %s\n", lineSplit[1])
		} else if lineSplit[0] == "RUN" {
			fmt.Printf("Running: %s\n", lineSplit[1])
			g.SynchronizedSend(lineSplit[1])
		} else if lineSplit[0] == "COLLECTIVE" {
			g.toggleCollectiveTracking(lineSplit[1])
		} else if lineSplit[0] == "CONFIG" {
			if lineSplit[1] == StopNonblockingModification {
				g.config[NonblockingModification] = false
			} else if lineSplit[1] == StartNonblockingModification {
				g.config[NonblockingModification] = true
			}
		}
	}

	processCommandsDone <- true
}

func (g *GdbInstance) toggleCollectiveTracking(coll string) {
	currVal, ok := g.trackedCollectives[coll]

	if !ok || !currVal {
		g.trackedCollectives[coll] = true
		g.SynchronizedSend(fmt.Sprintf("break internal_%s", coll))
	} else {
		g.trackedCollectives[coll] = false
		g.SynchronizedSend(fmt.Sprintf("clear internal_%s", coll))
	}
}

// On receiving a notification from gdb, the supplied `hook` will be run.
// Each hook is supposed to take input a notification and return a boolean
// in case it fails. A hook can have a name, so that hooks can be removed/added.
func (g *GdbInstance) AddNotificationHook(hookName string, hook func(notification map[string]interface{}) bool) {
	g.hooks[hookName] = hook
}

func (g *GdbInstance) RemoveNotificationHook(hookName string) {
	delete(g.hooks, hookName)
}

func (g *GdbInstance) handleNotifications(notification map[string]interface{}) {
	if notification["class"] == "library-loaded" || notification["class"] == "library-unloaded" {
		return
	}

	jsonString, err := json.MarshalIndent(notification, "", "  ")
	if err != nil {
		panic(err)
	}
	println(string(jsonString))

	// Handle Synchronization, in case we get a ^running, we should
	// wait for *stopped. Otherwise, we should carry on.
	switch notification["class"] {
	case "done":
	case "running":
		if len(notification) == 1 {
			<-g.breakpointHitNotification
		}

	case "stopped":
		g.breakpointHitNotification <- 1
		if _, ok := notification["payload"]; !ok {
			return
		}

		isBkpt, funcName, _ := analyzeStoppedProcess(notification["payload"].(map[string]interface{}))

		if isBkpt {
			go g.processBkpt(funcName)
			go func() {
				// Send variable information to the server whenever we hit a breakpoint,
				// so that latest information is present at the server whenever variable
				// window is examined *for this rank*.
				variables := g.getVariableInfo(funcName)
				g.internalInfoChan <- variables
			}()
		} else {
			payload, ok := notification["payload"].(map[string]interface{})
			if ok {
				if _, ok := payload["reason"]; !ok {
					return
				}

				if strings.HasPrefix(fmt.Sprintf("%v", payload["reason"]), "exited") ||
					strings.HasPrefix(fmt.Sprintf("%v", payload["reason"]), "signal-received") {
					// The process ended, either successfully or unsuccessfully
					endInfo := make(map[string]interface{})
					endInfo["KEY"] = "EXITED"
					endInfo["RANK"] = g.rank
					g.internalInfoChan <- endInfo
				} else if fmt.Sprintf("%v", payload["reason"]) == "watchpoint-trigger" {
					// Watchpoint triggered, writing on memory region which is still under
					// nonblocking transfer
					wpt, ok := payload["wpt"].(map[string]interface{})
					if ok {
						n, err := strconv.Atoi(fmt.Sprintf("%v", wpt["number"]))
						CheckError(err)
						if containsKey(g.hardwareBreakpoints, n) {
							if g.config[NonblockingModification].(bool) {
								newNotification := make(map[string]interface{})
								payload := "You just tried to modify a memory location which is " +
									"still under non-blocking transfer."
								newNotification["type"] = "console"
								newNotification["payload"] = payload
								g.hooks["ConsoleSendingHook"](newNotification)
							} else {
								g.SynchronizedSend("c")
							}
						}
					}
				}
			}
		}

	case "thread-selected":
		payload, ok := notification["payload"].(map[string]interface{})
		if ok {
			if _, fok := payload["frame"]; fok {
				go func() {
					variables := g.getVariableInfo()
					g.internalInfoChan <- variables
				}()
			}
		}

	case "breakpoint-created":
		payload, ok := notification["payload"].(map[string]interface{})
		if ok {
			breakpoint, bok := payload["bkpt"].(map[string]interface{})
			if bok {
				originalLocation, olok := breakpoint["original-location"].(string)
				if olok {
					res := breakpointStartLocation.FindAllStringSubmatch(originalLocation, 1)
					if res != nil {
						// Need to get the exact number assigned to the watchpoint created for
						// this request so that it can be removed when the transfer is finished
						locationStr := res[0][2]
						numStr, nok := breakpoint["number"].(string)
						if nok {
							num, err := strconv.Atoi(strings.TrimSpace(numStr))
							CheckError(err)
							if _, ok := g.hardwareBreakpoints[g.bufferToRequest[locationStr]]; ok {
								print("Reusing a request object which is not closed with" +
									" MPI_Wait. Please don't do that!!!\n")
								g.SynchronizedSend("delete " +
									strconv.Itoa(g.hardwareBreakpoints[g.bufferToRequest[locationStr]]))
							}
							mapWriteMux.Lock()
							g.hardwareBreakpoints[g.bufferToRequest[locationStr]] = num
							delete(g.bufferToRequest, locationStr)
							mapWriteMux.Unlock()
						}
					}
				}
			}
		}
	}

	// Run any custom hooks
	for _, hook := range g.hooks {
		// We don't really use the bool returned anywhere yet.
		hook(notification)
	}
}

func (g *GdbInstance) getVariableInfo(functionName ...string) map[string]interface{} {
	res := g.SynchronizedSend("-stack-list-locals --simple-values")
	if res["class"].(string) != "done" {
		return make(map[string]interface{})
	}

	payload := res["payload"].(map[string]interface{})
	array := payload["locals"].([]interface{})
	frameVars := make(map[string]interface{})
	frameVars["KEY"] = "FRAMEVARS"
	frameVars["VALUE"] = array
	frameVars["RANK"] = g.rank
	if len(functionName) > 0 {
		frameVars["FUNC"] = functionName[0]
	}
	return frameVars
}

func (g *GdbInstance) processBkpt(funcName string) {
	if strings.HasPrefix(funcName, "internal_") {
		if tracking, exists := g.trackedCollectives[strings.TrimPrefix(funcName, "internal_")]; !exists || !tracking {
			return
		}
		g.SynchronizedSend("finish")
		result := g.SynchronizedSend("-stack-list-variables 1")
		commS, _ := extractVariableFromResult(result, "comm")
		comm, _ := strconv.Atoi(commS)
		rankS, _ := extractVariableFromResult(result, "rank")
		rank, _ := strconv.Atoi(rankS)
		if comm != MPI_COMM_WORLD {
			return
		}
		result = g.SynchronizedSend("-stack-list-frames")
		c := CollectiveInfo{
			rank,
			getFileAndLineFromResult(result),
			strings.TrimPrefix(funcName, "internal_"),
		}
		g.cInfoChan <- c
		g.SynchronizedSend("continue")
	} else if funcName == "PDB_Request_start" {
		g.SynchronizedSend("next")
		g.SynchronizedSend("next")
		g.SynchronizedSend("next")
		result := g.SynchronizedSend("-stack-list-variables 1")
		sizeStr, _ := extractVariableFromResult(result, "size")
		size, _ := strconv.Atoi(sizeStr)
		countStr, _ := extractVariableFromResult(result, "count")
		count, _ := strconv.Atoi(countStr)
		request, _ := extractVariableFromResult(result, "request")
		buffer, _ := extractVariableFromResult(result, "buf")

		mapWriteMux.Lock()
		g.bufferToRequest[buffer] = request
		mapWriteMux.Unlock()

		// Example on how to set a watchpoint which tracks a memory region
		// watch *(void(*)[100])0x5555555592a0
		// Buffer is the starting location of the region
		watchpoint := fmt.Sprintf("watch *(void(*)[%d])(%s)", size*count, buffer)
		g.SynchronizedSend(watchpoint)
		g.SynchronizedSend("c")
	} else if funcName == "PDB_Request_end" {
		g.SynchronizedSend("next")
		g.SynchronizedSend("next")
		g.SynchronizedSend("next")
		g.SynchronizedSend("next")
		result := g.SynchronizedSend("-stack-list-variables 1")
		requestString, _ := extractVariableFromResult(result, "buf")
		request := strings.Split(requestString, " ")[1]
		request = strings.Trim(request, "\"")
		g.SynchronizedSend("delete " + strconv.Itoa(g.hardwareBreakpoints[request]))
		mapWriteMux.Lock()
		delete(g.hardwareBreakpoints, request)
		mapWriteMux.Unlock()
		g.SynchronizedSend("c")
	} else if funcName == "PMPI_Finalize" {
		if len(g.hardwareBreakpoints) != 0 {
			fmt.Printf("You have not called MPI_Wait for total %d requests. "+
				"Please check your code.", len(g.hardwareBreakpoints))
		}
		g.SynchronizedSend("c")
	}
}

// Send a command and wait for it to complete in gdb.
// This means that for an async command, we will wait till we
// get a stopped notification.
func (g *GdbInstance) SynchronizedSend(operation string, arguments ...string) map[string]interface{} {
	result, err := g.internal.Send(operation, arguments...)
	CheckError(err)
	g.handleNotifications(result)
	return result
}

func (g *GdbInstance) InitDefaultConfig() {
	g.config[NonblockingModification] = true
}

// Helper/Utility.

func getSoFilepath() string {
	dir := os.Getenv("PD_FILE_DIR")
	if dir == "" {
		dir_, err := os.Getwd()
		CheckError(err)
		dir = dir_
	}

	fname := fmt.Sprintf("%s/mpic.so", dir)
	_, err := os.Stat(fname)
	if err != nil && os.IsNotExist(err) {
		// Path is wrong for the shared library, throw error.
		log.Fatalln("Shared object does not exist, set PD_FILE_DIR to directory containing mpic.so")
	}

	return fname
}

func analyzeStoppedProcess(payload map[string]interface{}) (isBkpt bool, funcName string, bkptNo int) {
	reason_, ok := payload["reason"]

	if !ok {
		return false, "", -1
	}

	reason := reason_.(string)
	if reason != "breakpoint-hit" {
		return false, "", -1
	}

	frame := payload["frame"].(map[string]interface{})
	isBkpt = true
	funcName = frame["func"].(string)
	bkptNo, _ = strconv.Atoi(payload["bkptno"].(string))
	return
}

func containsKey(m map[string]int, num int) bool {
	for _, v := range m {
		if v == num {
			return true
		}
	}

	return false
}

func extractVariableFromResult(result map[string]interface{}, varname string) (string, bool) {
	payload := result["payload"].(map[string]interface{})
	variables := payload["variables"].([]interface{})
	for _, variable_ := range variables {
		variable := variable_.(map[string]interface{})
		if variable["name"].(string) == varname {
			return variable["value"].(string), true
		}
	}
	return "nil", false
}

func getFileAndLineFromResult(result map[string]interface{}) string {
	payload := result["payload"].(map[string]interface{})
	stack := payload["stack"].([]interface{})
	// We will look only at the 0th frame!
	top := stack[1].(map[string]interface{})
	frame := top["frame"].(map[string]interface{})
	file := frame["file"].(string)
	line := frame["line"].(string)
	return fmt.Sprintf("%s:%s", file, line)
}
