package utils

import (
	"log"
)

func CheckError(err error) {
	if err != nil {
		log.Fatalf("Fatal error: %s\n", err.Error())
	}
}

func Max(a int, b int) int {
	if a > b {
		return a
	} else {
		return b
	}
}
